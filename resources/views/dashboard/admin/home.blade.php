@extends('dashboard.layouts.master')

<style>
    .task-box{
        margin: 10px;
        padding: 10px;
        width: 100%;
        height: 180px;
        background-color: #057797;
        border: 3px solid #fff;
    }
    .task-box:hover { background-color: #484747; }


    .task-box-icon{
        text-align: center;
        margin: auto;
        width: 50%;
        padding-top: 10% !important;

        padding: 10px;
        color: aliceblue;
        font-size: 54px;
    }

    .task-box-text{
        color: #fff !important;
        text-align: center;
        font-size:18px;
    }

</style>

@section('content')
<div class="container">
    <div class="row">

        @yield('dashboard/sidebar')


        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"> <span class="glyphicon glyphicon-home"></span> Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="col-xs-12 col-md-4">
                        <a href="">
                            <div class="task-box">
                                <div class="task-box-icon">
                                    <span class="glyphicon glyphicon-home"></span>
                                </div>
                                <div class="task-box-text"> Home</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <a href="#">
                            <div class="task-box">
                                <div class="task-box-icon">
                                    <span class="glyphicon glyphicon-user"></span>
                                </div>
                                <div class="task-box-text"> Usuários</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <a href="">
                            <div class="task-box">
                                <div class="task-box-icon">
                                    <span class="glyphicon glyphicon-paperclip "></span>
                                </div>
                            <div class="task-box-text"> Anexos</div>
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
