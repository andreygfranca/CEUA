<style>

hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}
div.circle-avatar{
    /* make it responsive */
    max-width: 100%;
    width:100%;
    height:auto;
    display:block;
    /* div height to be the same as width*/
    padding-top:100%;

    /* make it a circle */
    border-radius:50%;

    /* Centering on image`s center*/
    background-position-y: center;
    background-position-x: center;
    background-repeat: no-repeat;

    /* it makes the clue thing, takes smaller dimension to fill div */
    background-size: cover;

    /* it is optional, for making this div centered in parent*/
    margin: 0 auto;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
#wrapper {
    padding-left: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

#wrapper.toggled {
    padding-left: 250px;
}

#sidebar-wrapper {
    z-index: 0;
    position: fixed;
    #top: 48px;
    left: 250px;
    width: 0;
    height: 100%;
    margin-left: -250px;
    overflow-y: auto;
    background: #222; 

    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

#wrapper.toggled #sidebar-wrapper {
    width: 250px;
}

#page-content-wrapper {
    width: 100%;
    position: absolute;
    padding: 15px;
}

#wrapper.toggled #page-content-wrapper {
    position: absolute;
    margin-right: -250px;
}



/* Sidebar Styles */

.sidebar-nav {
    position: absolute;
    top: 0;
    width: 250px;
    margin: 0;
    padding: 0;
    list-style: none;
}


.sidebar-nav  li {
    text-indent: 20px;
    line-height: 40px;
}

.sidebar-nav li a {
    display: block;
    text-decoration: none;
    color: #999999;
}

.sidebar-nav li a:hover {
    text-decoration: none;
    color: #fff;
    background: rgba(255, 255, 255, 0.2);
}

.sidebar-nav li a:active,
.sidebar-nav li a:focus {
    text-decoration: none;
}

.sidebar-nav>.sidebar-brand {
    height: 65px;
    font-size: 18px;
    line-height: 60px;
}

.sidebar-nav>.sidebar-brand a {
    color: #999999;
}

.sidebar-nav>.sidebar-brand a:hover {
    color: #fff;
    background: none;
}

@media(min-width:768px) {
    #wrapper {
        padding-left: 250px;
    }

    #wrapper.toggled {
        padding-left: 200;
    }

    #sidebar-wrapper {
        width: 250px;
        top:-3px;
    }

    #wrapper.toggled #sidebar-wrapper {
        width: 0;
    }

    #page-content-wrapper {
        padding: 20px;
        position: relative;
    }

    #wrapper.toggled #page-content-wrapper {
        position: relative;
        margin-right: 0;
    }
}
</style>


<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav" id="sidenav01">
            <li class="sidebar-brand">
                <a href="/painel/">
                        
                    </a>
            </li>
            <li>
                {{-- <div class="circle-avatar" style="background-image:url(/{{ Auth::user()->name }})"></div> --}}

            </li>
            <li>
                <a  href="#"> <span class="glyphicon glyphicon-usd"></span> Projetos </a>
            </li>
            <li>
                <a href="#"><span class="glyphicon glyphicon-list-alt"></span> Relatórios </a>
            </li>
            <li>
                <a href="#"><span class="glyphicon glyphicon-wrench"></span> Gestão de Usuários </a>
            </li>
            {{-- @if (Auth::user()->level > 1)
                <li>
                    <a href="/painel/config/principal"><span class="glyphicon glyphicon-globe"></span>  Informações do site</a>
                </li>
                <li>
                    <a href="/painel/noticias/principal"><span class="glyphicon glyphicon-pencil"></span> Gestão de Notícias </a>
                </li>
                <li>
                    <a href="/painel/usuarios/principal"><span class="glyphicon glyphicon-user"></span> Gestão de Usuários</a>
                </li>

                <li>
                    <a href="/painel/galeria/principal"><span class="glyphicon glyphicon-picture"></span> Galeria de Fotos</a>
                </li>
            @endif --}}
        </ul>
    </div>
</div>
<!-- /#sidebar-wrapper -->
