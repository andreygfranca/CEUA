@extends('dashboard.layouts.master')
@section('content')

<div class="container">
  <div class="row">
    @yield('dashboard/sidebar')
    <div class="col-md-10 col-md-offset-2">
      <div class="panel panel-default" style="margin-top: 80px;">
        <div class="panel-heading"> <span class="glyphicon glyphicon-home"></span> Projeto de Pesquisa</div>
        <div class="panel-body">
          @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
          @endif

          <div class="col-xs-12 col-md-4">
            <a href="/pesquisador/formulario-protocolo">
              <div class="task-box">
                <div class="task-box-icon">
                  <span class="glyphicon glyphicon-list-alt"></span>
                </div>
                <div class="task-box-text"> Formulário de Protocolo</div>
              </div>
            </a>
          </div>

          <div class="col-xs-12 col-md-4">
            <a href="/pesquisador/projetos">
              <div class="task-box">
                <div class="task-box-icon">
                  <span class="glyphicon glyphicon-book   "></span>
                </div>
                <div class="task-box-text"> Projetos Cadastrados</div>
              </div>
            </a>
          </div>

          {{-- <div class="col-xs-12 col-md-4">
            <a href="">
              <div class="task-box">
                <div class="task-box-icon">
                  <span class="glyphicon glyphicon-paperclip "></span>
                </div>
                <div class="task-box-text"> Anexos</div>
              </a>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection