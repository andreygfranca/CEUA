@extends('dashboard.layouts.master')
<script src="{{asset('js/jquery.min.js')}}"></script>

@section('content')
<div class="container">
  <div class="row">

    @yield('dashboard/sidebar')


    <div class="col-md-10 col-md-offset-2">
      <div class="panel panel-default" style="margin-top:80px;">
          <div class="panel-heading"> 
            <span class="glyphicon glyphicon-home"></span> 
            Seus Projetos de Pesquisa
          </div>

          <div class="panel-body">
            @if (session('status'))
              <div class="alert alert-success">
                {{ session('status') }}
              </div>
            @endif

            <table class="table">
					   	<thead>
					      <tr>
					        <th>Título</th>
					        <th>Responsável</th>
					        <th>Email</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr>
					        <td>John</td>
					        <td>Doe</td>
					        <td>john@example.com</td>
					      </tr>
					      <tr>
					        <td>Mary</td>
					        <td>Moe</td>
					        <td>mary@example.com</td>
					      </tr>
					      <tr>
					        <td>July</td>
					        <td>Dooley</td>
					        <td>july@example.com</td>
					      </tr>
					    </tbody>
					  </table>

          </div>

           
@endsection

