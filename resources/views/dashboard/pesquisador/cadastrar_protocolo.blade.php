@extends('dashboard.layouts.master')
<script src="{{asset('js/jquery.min.js')}}"></script>

@section('content')
<div class="container">
  <div class="row">

    @yield('dashboard/sidebar')


    <div class="col-md-10 col-md-offset-2">
      <div class="panel panel-default" style="margin-top:80px;">
          <div class="panel-heading"> 
            <span class="glyphicon glyphicon-home"></span> 
            Projeto de Pesquisa
          </div>

          <div class="panel-body">
            @if (session('status'))
              <div class="alert alert-success">
                {{ session('status') }}
              </div>
            @endif

            <form action="">
              <h3 class="text-center">Responsável</h3>
              <h4 class="text-center"> Pesquisador Coordenador do projeto no SIGAA</h4>
              <hr>
              <div class="form-group">
                <label for="coordNome">Nome completo:</label>
                <input type="text" class="form-control" id="coordNomeId" name="coordNome" placeholder="Digite o nome completo" >

                <label for="coordInstituicao">Instituição/Unidade:</label>
                <input type="text" class="form-control" id="coordInstituicaoId" name="coordInstituicao">

                <label for="coordDepartamento">Departamento:</label>
                <input type="text" class="form-control" id="coordDepartamentoId" name="coordDepartamento">

                <label for="coordVinculo">Vínculo com a Instituição:</label>
                <input type="text" class="form-control" id="coordVinculoId" name="coordVinculo">

                <label for="coordTel">Telefone:</label>
                <input type="tel" class="form-control" name="coordTel">

                <label for="coordEmail">Email</label>
                <input type="email" class="form-control" name="coordEmail">

                <label for="coordLattes">Currículo <i>lattes</i> (endereço):</label>
                <input type="text" class="form-control" name="coordLattes">

                <label for="coordExp">Experiência Prévia com a área da pesquisa (sim/não) e Tempo</label>
                <input type="text" class="form-control" name="coordExp">
              </div>




              <h4 class="text-center">Pesquisador responsável (mestrando, doutorando ou coordenador do projeto)</h4>
              <div class="form-group">
                <label for="respNome">Nome do responsável pelo projeto:</label>
                <input type="text" class="form-control" id="respNomeId" name="respNome" placeholder="Digite o nome completo do responsável" >

                <label for="respInstituicao">Instituição/Unidade:</label>
                <input type="text" class="form-control" id="respInstituicaoId" name="respInstituicao">

                <label for="respDepartamento">Departamento:</label>
                <input type="text" class="form-control" id="respDepartamentoId" name="respDepartamento">

                <label for="respVinculo">Vínculo com a Instituição:</label>
                <input type="text" class="form-control" id="respVinculoId" name="respVinculo">

                <label for="respTel">Telefone:</label>
                <input type="tel" class="form-control" name="respTel">

                <label for="respEmail">Email</label>
                <input type="email" class="form-control" name="respEmail">

                <label for="respLattes">Currículo <i>lattes</i> (endereço):</label>
                <input type="text" class="form-control" name="respLattes">

                <label for="respExp">Experiência Prévia com a área da pesquisa (sim/não) e Tempo</label>
                <input type="text" class="form-control" name="respExp">
              </div>
              
              <div class="form-group">
                <hr>
                <h3 class="text-center">Colaboradores</h3>
                
                <div id="TextBoxesGroup" ></div>

                <button type="button" class="btn btn-primary" id="addButton">Adcionar Colaboradores</button>
                <button type="button" class="btn btn-danger" id="removeButton">Remover Colaborador</button><br>
                <small>Clique no botão para adcionar um novo colaborador</small>

                <br>
              </div>




              <div class="form-group">
                <hr>
                <label for="medicoVeterinario"> Médico Veterinário da Pesquisa </label>
                <div class="radio">
                  <label><input type="radio" id="vetRadioSim" name="optVet" value="vetRadioSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="vetRadioNao" name="optVet" value="vetRadioNao">Não</label>
                </div>

                <div id="veterinarioForm">
                  <div class="col-md-6">
                  <label for="vetNome">Nome Completo</label>
                  <input type="text" class="form-control">
                </div>
                <div class="col-md-6">

                  <label for="vetNumeroCrmv">Número CRMV</label>
                  <input type="text" class="form-control">
                </div>

                  <div class="col-md-6">
                    <label for="vetEmail">E-mail</label>
                    <input type="text" class="form-control" name="vetEmail">
                  </div>
                  <div class="col-md-6" style="margin-bottom: 30px;">
                    <label for="vetTelefone">Telefone</label>
                    <input type="tel" name="vetTel" class="form-control">
                  </div>
                </div>
                
                <div id="justificativa"> 
                  <label for="justificativa">Justificativa</label>
                  <textarea class="form-control" name="justificativa"  cols="10" rows="4"></textarea>
                </div>
              </div>




              <div class="form-group">
                <h4 class="text-center">Resumo do Projeto de Pesquisa</h4>
                <textarea class="form-control" name="resumo" id="resumoId" cols="30" rows="10"></textarea>

                <h4 class="text-center">Objetivos (geral e específicos)</h4>
                <textarea class="form-control" name="objetivos" id="objetivosId" cols="30" rows="10"></textarea>

                <h4 class="text-center">Justificativa e Relevância do Projeto de Pesquisa</h4>
                <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>

                <h4 class="text-center"> Possibilidade de Métodos Alternativos</h4>
                <textarea class="form-control" name="metodos-alternativos" id="metodosId" cols="30" rows="10"></textarea>
              </div> 

              <p><strong>****Parte dos animais, falta cadastrar animais</strong></p>

              <div class="form-group">
                <label for="silvestre-opt">Animal silvestre?</label>
                <div class="radio">
                  <label><input type="radio" id="optSilvestreSim" name="optSilvestre" value="silvestreSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optSilvestreNao" name="optSilvestre" value="silvestreNao">Não</label>
                </div>

                <div id="numSisbio">
                  <label for="numSisbio">Número de protocolo SISBIO</label>
                  <input type="text" class="form-control" name="numSisbio" >
                </div>
                <div id="captura">
                  <label for="captura">Métodos de captura (somente em caso de uso de animais silvestres)</label>
                  <input type="text" class="form-control" name="metodoCaptura">
                </div>



                <label for="proced-opt">Outra Procedência?</label>
                <div class="radio">
                  <label><input type="radio" id="optProcedSim" name="optProced" value="procedSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optProcedNao" name="optProced" value="procedNao">Não</label>
                </div>

                <div id="procedencia">
                  <label for="procedencia">Qual?</label>
                  <input type="text" class="form-control" name="qualProcedencia">
                </div>

                <label for="genModifcado">O animal é geneticamente modificado? </label>
                <div class="radio">
                  <label><input type="radio" id="optModifSim" name="optModif" value="Sim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optModifNao" name="optModif" value="modifNao">Não</label>
                </div>
                <div id="ctnbio">
                  <label for="ctnbio">Número de protocolo CTNBio</label>
                  <input type="text" class="form-control" name="ctnbio">
                </div>

                <label for="justNumAnimais">
                  Planejamento estatístico/Delineamento experimental que justifique o número de animais utilizados na pesquisa
                </label>
                <textarea class="form-control" name="justNumAnimais" cols="30" rows="5"></textarea>
                
                <label for="invisibilidade">Grau de invasividade</label>
                <input type="text" class="form-control" name="invisibilidade">

                <label for="materiaisBiologicos">
                  Os materiais biológicos destes exemplares serão usados em outros projetos? Quais? Se já aprovado pela CEUA, mencionar o número do protocolo.
                </label>
                <input type="text" class="form-control" name="meteriaisBiologicos">
              </div>

              <hr>

              <div class="form-group">
                <h4 class="text-center">Condições de alimentação e alojamento dos animais</h4>
                <label for="fonteAlimAgua">Alimentação e fonte de água</label>
                <input type="text" class="form-control" name="fonteAlimAgua">
                <small>Comentar obrigatoriamente sobre a dieta, regime alimentar e hídrico e as demais condições que forem particulares à espécie</small>

                <label for="localAnimal">Local onde será mantido o animal</label>
                <input type="text" class="form-control" name="localAnimal">

                <label for="unidade">Unidade (se na UFG) ou Endereço (se fora da UFG) </label>
                <input type="text" class="form-control" name="unidade">

                <label for="macroambiente">Macroambiente (temperatura, umidade, ventilação, luminosidade) </label>
                <input type="text" class="form-control" name="macroambiente">

                <label for="microambiente">Microambiente (local de alojamento, área, medida)</label>
                <input type="text" class="form-control" name="microambiente">

                <label for="animaisGaiola">Número de animais por gaiola/baia/piquete</label>
                <input type="text" class="form-control" name="animaisGaiola">

                <label for="tipoCama">Tipo de cama (maravalha, estrado ou outro) </label>
                <input type="text" class="form-control" name="tipoCama">

                <label for="EnriqAmbiental">Enriquecimento Ambiental</label>
                <input type="text" class="form-control" name="EnriqAmbiental">
              </div>

              <hr>

              <div class="form-group">
                <h3 class="text-center">Procedimentos experimentais do projeto de pesquisa</h3>
                <small>No campo “fármaco” de todos os itens a seguir, deve-se informar o(s) nome(s) do(s) princípio(s) ativo(s) com suas respectivas Denominação Comum Brasileira (DCB) ou Denominação Comum Internacional (DCI). Lista das DCB disponível em: link http://www.anvisa.gov.br/medicamentos/dcb/lista_dcb_2007.pdf .</small>

                <label for="remedios-opt">Uso de fármacos anestésicos</label>
                <div class="radio">
                  <label><input type="radio" id="optRemediosSim" name="optRemedios" value="remediosSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optRemediosNao" name="optRemedios" value="remediosNao">Não</label>
                </div>

                <div id="remediosJust">
                  <label for="remediosJust">Justifique</label>
                  <input type="text" class="form-control" name="remediosJust">
                </div>
            
                <div id="listaFarmaco"></div>
                <button type="button" class="btn btn-primary" id="btnAddRemedio">Adcionar Fármaco</button>
                <button type="button" class="btn btn-danger" id="btnRemoveRemedio">Remover Fármaco</button><br><br>


                {{-- Fármacos --}}

                <label for="relaxante">Uso de relaxante muscular</label>
                <div class="radio">
                  <label><input type="radio" id="optRelaxanteSim" name="optRelaxante" value="relaxanteSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optRelaxanteNao" name="optRelaxante" value="relaxanteNao">Não</label>
                </div>

                <div id="relaxanteJust">
                  <label for="relaxanteJust">Justifique</label>
                  <input type="text" class="form-control" name="relaxanteJust">
                </div>

                <div id="listaRelaxante"></div>
                <button type="button" class="btn btn-primary" id="btnAddRelaxante">Adcionar Relaxante</button>
                <button type="button" class="btn btn-danger" id="btnRemoveRelaxante">Remover Relaxante</button><br><br>

                {{-- Analgésicos --}}

                <label for="relaxante"> Uso de fármacos analgésicos</label>
                <div class="radio">
                  <label><input type="radio" id="optAnalgesicoSim" name="optAnalgesico" value="analgesicoSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optAnalgesicoNao" name="optAnalgesico" value="analgesicoNao">Não</label>
                </div>

                <div id="analgesicoJust">
                  <label for="analgesicoJust">Justifique</label>
                  <input type="text" class="form-control" name="analgesicoJust">
                </div>

                <div id="listaAnalgesico"></div>
                <button type="button" class="btn btn-primary" id="btnAddAnalgesico">Adcionar Analgesico</button>
                <button type="button" class="btn btn-danger" id="btnRemoveAnalgesico">Remover Analgesico</button><br><br>


                <label for="contencaoAnimal">Contenção do animal</label>
                <div class="radio">
                  <label><input type="radio" id="optContencaoSim" name="optContencao" value="contencaoSim">Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optContencaoNao" name="optContencao" value="contencaoNao">Não</label>
                </div>

                <div id="contencaoJust">
                  <label for="analgesicoJust">Justifique</label>
                  <input type="text" class="form-control" name="analgesicoJust">
                </div>

                <h4 class="text-center">Condições alimentares </h4>
                <label for="jejum">Jejum</label>
                <div class="radio">
                  <label><input type="radio" id="optJejumSim" name="optJejum" value="jejumSim">Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optJejumNao" name="optJejum" value="jejumNao">Não</label>
                </div>

                <div id="jejumDuracao">
                  <label for="jejumDuracao">Duração em horas:</label>
                  <input type="text" class="form-control" name="jejumDuracao">
                </div>

                <label for="jejum">Restrição hídrica</label>
                <div class="radio">
                  <label><input type="radio" id="optRestricaoHidSim" name="optRestricaoHid" value="RestricaoHidSim">Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optRestricaoHidNao" name="optRestricaoHid" value="RestricaoHidNao">Não</label>
                </div>

                <div id="restricaoHidDuracao">
                  <label for="restricaoHidDuracao">Duração em horas:</label>
                  <input type="text" class="form-control" name="restricaoHidDuracao">
                </div>

                <label for="cirurgia">Cirurgia</label>
                <div class="radio">
                  <label><input type="radio" id="optCirurgiaSim" name="optCirurgia" value="cirurgiaSim">Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optCirurgiaNao" name="optCirurgia" value="cirurgiaNao">Não</label>
                </div>

                <div id="cirurgiaForm">
                  <div class="radio-inline">
                  <label><input type="radio" id="optCirurgiaQuantUnica" name="optCirurgiaQuant" value="cirurgiaQuantUnica">Única</label>
                  </div>
                  <div class="radio-inline">
                    <label><input type="radio" id="optCirurgiaQuantMult" name="optCirurgiaQuant" value="cirurgiaMult">Múltipla</label>
                  </div>

                  <div id="quaisCirurgias">
                    <label for="quaisCirurgias">Qual (is)?</label>
                    <input type="text" class="form-control" name="quaisCirurgias">

                    <label for="atoCirurgico">No mesmo ato cirúrgico ou em atos diferentes? </label>
                    <input type="text" class="form-control" name="atoCirurgico">
                  </div>
                </div>

                <hr>
                <h4 class="text-center">Pós-operatório</h4>
                <label for="obsRecuperacao"> Observação da recuperação</label>
                <div class="radio">
                <label><input type="radio" id="optObsRecuperacaoSim" name="optObsRecuperacao" value="obsRecuperacaoSim">Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optObsRecuperacaoNao" name="optObsRecuperacao" value="obsRecuperacaoSim">Não</label>
                </div>

                <div id="obsRecuperacaoForm">
                  <label for="perRecuperacao">Período de observação (em horas):</label>
                  <input type="text" class="form-control" name="perRecuperacao">

                  <label for="metObsRecuperacao">Descrever metodologia de observação de recuperação: </label>
                  <input type="text" class="form-control" name="metObsRecuperacao">
                </div>


                <label for="analgesiaOpt">Uso de analgesia</label>
                <div class="radio">
                  <label><input type="radio" id="optAnalgesiaSim" name="optAnalgesia" value="analgesiaSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optAnalgesiaNao" name="optAnalgesia">Não</label>
                </div>

                <div id="analgesiaJust">
                  <label for="analgesiaJust">Justifique</label>
                  <input type="text" class="form-control" name="analgesiaJust">
                </div>
            
                <div id="listaAnalgesia"></div>
                <button type="button" class="btn btn-primary" id="btnAddAnalgesia">Adcionar Analgesia</button>
                <button type="button" class="btn btn-danger" id="btnRemoveAnalgesia">Remover Analgesia</button><br><br>


                <label for="outrosCuidades">Outros cuidados pós-operatórios </label>
                <div class="radio">
                  <label><input type="radio" id="optOutrosCuidSim" name="optOutrosCuid" value="outrosCuidSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optOutrosCuidNao" name="optOutrosCuid" value="outrosCuidNao">Não</label>
                </div>

                <div id="outrosCuidForm">
                  <label for="outrosCuidDesc">Descrição:</label>
                  <input type="text" class="form-control" name="outrosCuid">
                </div>


                <label for="expInoAdm">Exposição / Inoculação / Administração</label>
                <div class="radio">
                  <label><input type="radio" id="optExpInoAdmSim" name="optExpInoAdm" value="expInoAdmSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optExpInoAdmNao" name="optExpInoAdm" value="expInoAdmNao">Não</label>
                </div>

                <div id="expInoAdmForm">
                  <label for="farmaco">Fármaco/Outros*</label>
                  <input type="text" class="form-control" name="expInoAdm_Farmacos">
                  <small>* extratos vegetais, vacinas, entre outros.</small><br>
                  <label for="dose">Dose</label>
                  <input type="text" class="form-control" name="expInoAdm_Dose">
                  <label for="expInoAdm_ViaAdm">Via de administração</label>
                  <input type="text" class="form-control" name="expInoAdm_ViaAdm">
                  <label for="expInoAdm_Frequencia">Frequência</label>
                  <input type="text" class="form-control" name="expInoAdm_Frequencia">
                </div>

                <label for="expInoAdmInfec">Exposição / Inoculação / Administração de agentes infecciosos</label>
                <div class="radio">
                  <label><input type="radio" id="optExpInoAdmInfecSim" name="optExpInoAdmInfec" value="expInoAdmInfecSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optExpInoAdmInfecNao" name="optExpInoAdmInfece" value="expInoAdmInfecNao">Não</label>
                </div>

                <div id="expInoAdmInfecForm">
                  <label for="expInoAdmInfec_Especie">Qual (espécie)</label>
                  <input type="text" class="form-control" name="expInoAdmInfec_Especie">
                  <label for="expInoAdmInfec_PotZoo">Potencial zoonótico</label>
                  <input type="text" class="form-control" name="expInoAdmInfec_PotZoo">
                  <label for="expInoAdmInfec_ViaInf">Via utilizada para infecção</label>
                  <input type="text" class="form-control" name="expInoAdmInfec_ViaInf">
                  <label for="expInoAdmInfec_Frequencia">Frequência</label>
                  <input type="text" class="form-control" name="expInoAdmInfec_Frequencia">
                </div>
              </div> {{-- Form Group --}}


              <div class="form-group">

                <h3 class="text-center">Extração de Materiais Biológicos</h3>    
                <label for="extMateriaisBio">Extração de Materiais Biológicos</label>
                <div class="radio">
                  <label><input type="radio" id="optExtMateriaisBioSim" name="optExtMateriaisBio" value="extMateriaisBioSim" >Sim</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="optExtMateriaisBioNao" name="optExtMateriaisBio" value="extMateriaisBioNao">Não</label>
                </div>

                <div id="listaExtMateriaisBio"></div>
                <button type="button" class="btn btn-primary" id="btnAddMaterial">Adcionar Material</button>
                <button type="button" class="btn btn-danger" id="btnRemoveMaterial">Remover Material</button><br><br>

                <h3 class="text-center">Pontos Finais Humanitários</h3>
                <label for="">Cuidados adicionais para minimizar e/ou evitar dor e sofrimento dos animais em experimentação</label>
                <input type="text" class="form-control" name="pontosFinaisHumanitarios">

                <label for="riscosPesqAlunos">Riscos aos Pesquisadores e Alunos e Métodos para previni-los</label>
                <input type="text" class="form-control" name="riscosPesqAlunos">

                <h4 class="text-center">Destino e Eutanásia dos Animais</h4>

                <label for="eutanasiaAnimais">Destino dos animais após o experimento</label>
                <input type="text" class="form-control" name="eutanasiaAnimais">
                <br>
                <strong>  Método de eutanásia </strong><br>
                <label for="eutanasiaDesc">Descrição</label>
                <input type="text" name="eutanasiaDesc" class="form-control">

                <label for="eutanasiaSubDose">Substância, dose, via</label>
                <input type="text" name="eutanasiaSubDose" class="form-control">

                <label for="justMetodoRest">Caso método restrito, justifique:</label>
                <input type="text" name="justMetodoRest" class="form-control">

                <label for="descarteCarcaca">Forma de descarte da carcaça</label>
                <input type="text" class="form-control" name="descarteCarcaca">

                <label for="resumoProcMetodo">Resumo Procedimento Metodológico (relatar todos os procedimentos com os animais)</label>

                <textarea class="form-control" name="resumoProcMetodo" id="resumoProcMetodo" cols="30" rows="10"></textarea>

                <label for="cronograma">Cronograma Mensal</label>
                <textarea class="form-control" name="cronograma" id="cronograma" cols="30" rows="10"></textarea>
              </div>


            </form>


          </div>

      </div>
    </div>
  </div>
</div>




<script type="text/javascript">
  $('#veterinarioForm').hide();
  $('#justificativa').hide();
  $('#procedencia').hide(); 
  $('#numSisbio').hide();
  $('#ctnbio').hide();
  $('#captura').hide();
  $('#remediosJust').hide();
  $('#relaxanteJust').hide();
  $('#analgesicoJust').hide();
  $('#analgesiaJust').hide();
  $('#btnAddRemedio').hide();
  $('#btnRemoveRemedio').hide();
  $('#btnAddRelaxante').hide();
  $('#btnRemoveRelaxante').hide();
  $('#btnAddAnalgesico').hide();
  $('#btnRemoveAnalgesico').hide();
  $('#btnAddAnalgesia').hide();
  $('#btnRemoveAnalgesia').hide();
  $('#btnAddMaterial').hide();
  $('#btnRemoveMaterial').hide();
  $('#contencaoJust').hide();
  $('#jejumDuracao').hide();
  $('#restricaoHidDuracao').hide();
  $('#cirurgiaForm').hide();
  $('#quaisCirurgias').hide();
  $('#obsRecuperacaoForm').hide();
  $('#outrosCuidForm').hide();
  $('#expInoAdmForm').hide();
  $('#expInoAdmInfecForm').hide();


  // Adciona mais colaboradores:
  var counter = 1;
  $("#addButton").click(function () {
    if(counter>10){
      alert("No máximo 10 colaboradores são permitidos.");
      return false;
    }
    var newTextBoxDiv = $(document.createElement('div'))
      .attr("id", 'TextBoxDiv' + counter);
    newTextBoxDiv.after().html('<label>Nome Completo #'+ counter + ' : </label>' +
      '<input type="text" class="form-control" name="colNome' + counter +
      '" id="colNome' + counter + '" value="" >' +

      '<label>Instituição #'+ counter + ' : </label>' +
      '<input type="text" class="form-control" name="colInst' + counter +
      '" id="colInst' + counter + '" value="" >' + 

      '<label>Nível acadêmico #'+ counter + ' : </label>' +
      '<input type="text" class="form-control" name="colNivel' + counter +
      '" id="colNivel' + counter + '" value="" >'+

      '<label>Currículo lattes (endereço) #'+ counter + ' : </label>' +
      '<input type="text" class="form-control" name="colLattes' + counter +
      '" id="colLattes' + counter + '" value="" >'+

      '<label>Telefone#'+ counter + ' : </label>' +
      '<input type="tel" class="form-control" name="colTel' + counter +
      '" id="colTel' + counter + '" value="" >' +

      '<label>E-mail #'+ counter + ' : </label>' +
      '<input type="email" class="form-control" name="colEmail' + counter +
      '" id="colEmail' + counter + '" value="" >'+


      '<label>Função da Pesquisa#'+ counter + ' : </label>' +
      '<input type="text" class="form-control" name="colFunc' + counter +
      '" id="colFunc' + counter + '" value="" >' + '<hr>');
    
    newTextBoxDiv.appendTo("#TextBoxesGroup");
    counter++;
  });


  //TODO: Remover Colaboradores..

  $("#removeButton").click(function () {
    if(counter==1){
      alert("Não há mais colaboradores para removerTextBoxesGroup");
      return false;
    }
    counter--;
    $("#TextBoxDiv" + counter).remove();
  });


  var div = document.getElementById('veterinarioForm');

  $('#vetRadioNao').click(function() {
    $('#veterinarioForm').hide();
    $('#justificativa').show();
    // div.style.visibility = 'hidden';
  });

  $('#vetRadioSim').click(function() {
    $('#veterinarioForm').show();
    $('#justificativa').hide();
  });


  $('#optSilvestreSim').click(function() {
    $('#numSisbio').show();
    $('#captura').show();
  });

  $('#optSilvestreNao').click(function() {
    $('#captura').hide();
    $('#numSisbio').hide();
  });

  $('#optProcedSim').click(function(){
    $('#procedencia').show();
  });
  $('#optProcedNao').click(function(){
    $('#procedencia').hide();
  });

  $('#optModifSim').click(function(){
    $('#ctnbio').show();
  });
  $('#optModifNao').click(function(){
    $('#ctnbio').hide();
  });

  $('#optRemediosSim').click(function(){
    $('#btnAddRemedio').show();
    $('#btnRemoveRemedio').show();
    $('#remediosJust').hide();
  });
  $('#optRemediosNao').click(function(){
    $('#remediosJust').show();
    $('#btnAddRemedio').hide();
    $('#btnRemoveRemedio').hide();
    console.log(farmacoCount);
    for (var i = 0; i < farmacoCount; i++) {
      console.log('removing #farmacoForm'+i);
      $('#farmacoForm'+i).remove();
    }
    farmacoCount = 1;
  });

   $('#optRelaxanteSim').click(function(){
    $('#btnAddRelaxante').show();
    $('#btnRemoveRelaxante').show();
    $('#relaxanteJust').hide();
  });
  $('#optRelaxanteNao').click(function(){
    $('#relaxanteJust').show();
    $('#btnAddRelaxante').hide();
    $('#btnRemoveRelaxante').hide();
    console.log(relaxanteCount);
    for (var i = 0; i < relaxanteCount; i++) {
      console.log('removing #relaxanteForm'+i);
      $('#relaxanteForm'+i).remove();
    }
    farmacoCount = 1;
  });

   $('#optAnalgesicoSim').click(function(){
    $('#btnAddAnalgesico').show();
    $('#btnRemoveAnalgesico').show();
    $('#analgesicoJust').hide();
  });
  $('#optAnalgesicoNao').click(function(){
    $('#analgesicoJust').show();
    $('#btnAddAnalgesico').hide();
    $('#btnRemoveAnalgesico').hide();
    console.log(analgesicoCount);
    for (var i = 0; i < analgesicoCount; i++) {
      console.log('removing #analgesicoForm'+i);
      $('#analgesicoForm'+i).remove();
    }
    farmacoCount = 1;
  });

  //ANALGESIA
  $('#optAnalgesiaSim').click(function(){
    $('#btnAddAnalgesia').show();
    $('#btnRemoveAnalgesia').show();
    $('#analgesiaJust').hide();
  });
  $('#optAnalgesiaNao').click(function(){
    $('#analgesiaJust').show();
    $('#btnAddAnalgesia').hide();
    $('#btnRemoveAnalgesia').hide();
    console.log(analgesiaCount);
    for (var i = 0; i < analgesiaCount; i++) {
      console.log('removing #analgesiaForm'+i);
      $('#analgesiaForm'+i).remove();
    }
    analgesiaCount = 1;
  });


  // MATERIAL
  $('#optExtMateriaisBioSim').click(function(){
    $('#btnAddMaterial').show();
    $('#btnRemoveMaterial').show();
    // $('#analgesiaJust').hide();
  });
  $('#optExtMateriaisBioNao').click(function(){
    // $('#analgesiaJust').show();
    $('#btnAddMaterial').hide();
    $('#btnRemoveMaterial').hide();
    console.log(materialCount);
    for (var i = 0; i < materialCount; i++) {
      console.log('removing #materialForm'+i);
      $('#materialForm'+i).remove();
    }
    materialCount = 1;
  });


  $('#optContencaoSim').click(function() {
    $('#contencaoJust').show();
  });
  $('#optContencaoNao').click(function() {
    $('#contencaoJust').hide();
  });

  $('#optJejumSim').click(function() {
    $('#jejumDuracao').show();
  });
  $('#optJejumNao').click(function() {
    $('#jejumDuracao').hide();
  });

  $('#optRestricaoHidSim').click(function() {
    $('#restricaoHidDuracao').show();
  });
  $('#optRestricaoHidNao').click(function() {
    $('#restricaoHidDuracao').hide();
  });

 $('#optCirurgiaSim').click(function() {
    $('#cirurgiaForm').show();
  });
  $('#optCirurgiaNao').click(function() {
    $('#cirurgiaForm').hide();
  });
  

  $('#optCirurgiaQuantMult').click(function(){
    $('#quaisCirurgias').show();
  });
  $('#optCirurgiaQuantUnica').click(function(){
    $('#quaisCirurgias').hide();
  });

  $('#optObsRecuperacaoSim').click(function(){
    $('#obsRecuperacaoForm').show();
  });

  $('#optObsRecuperacaoNao').click(function(){
    $('#obsRecuperacaoForm').hide();
  });

  $('#optOutrosCuidSim').click(function(){
    $('#outrosCuidForm').show();
  });

  $('#optOutrosCuidNao').click(function(){
    $('#outrosCuidForm').hide();
  });


  $('#optExpInoAdmSim').click(function(){
    $("#expInoAdmForm").show();
  });
  $('#optExpInoAdmNao').click(function(){
    $("#expInoAdmForm").hide();
  });

  $('#optExpInoAdmInfecSim').click(function(){
    $("#expInoAdmInfecForm").show();
  });
  $('#optExpInoAdmInfecNao').click(function(){
    $("#expInoAdmInfecForm").hide();
  });

  // FARMACOS
  let farmacoCount = 1;
  $('#btnAddRemedio').on("click", function(){
    let listaFarmaco = '<div id="farmacoForm' + farmacoCount +'">\
                  <label for="farmaco">Fármaco#' + farmacoCount +'</label> \
                  <input type="text" class="form-control" name="nomeFarmaco' + farmacoCount +'">\
                  <label for="doseFarmaco">Dose (UI ou mg/kg) #' + farmacoCount +'</label>\
                  <input type="text" class="form-control" name="doseFarmaco' + farmacoCount +'">\
                  <label for="viaAdministração">Via de Administração#' + farmacoCount +'</label>\
                  <input type="text" class="form-control" name="viaAdministração' + farmacoCount +'">';
    $('#listaFarmaco').append(listaFarmaco);

    farmacoCount+=1
    console.log(farmacoCount);
  });
  
  $('#btnRemoveRemedio').on("click", function(){
    if (farmacoCount>1) {
      farmacoCount--;
      $('#farmacoForm'+farmacoCount).remove();
    }
  });

    // Relaxante
  let relaxanteCount = 1;
  $('#btnAddRelaxante').on("click", function(){
    let listaFarmaco = '<div id="relaxanteForm' + relaxanteCount +'">\
                  <label for="farmaco">Fármaco#' + relaxanteCount +'</label> \
                  <input type="text" class="form-control" name="nomeFarmaco' + relaxanteCount +'">\
                  <label for="doseFarmaco">Dose (UI ou mg/kg) #' + relaxanteCount +'</label>\
                  <input type="text" class="form-control" name="doseFarmaco' + relaxanteCount +'">\
                  <label for="viaAdministração">Via de Administração#' + relaxanteCount +'</label>\
                  <input type="text" class="form-control" name="viaAdministração' + relaxanteCount +'">';
    $('#listaRelaxante').append(listaFarmaco);

    relaxanteCount+=1
    console.log(relaxanteCount);
  });
  
  $('#btnRemoveRelaxante').on("click", function(){
    if (relaxanteCount>1) {
      relaxanteCount--;
      $('#relaxanteForm'+relaxanteCount).remove();
    }
  });

    // Analgesico
  let analgesicoCount = 1;
  $('#btnAddAnalgesico').on("click", function(){
    let listaAnalgesico = '<div id="analgesicoForm' + analgesicoCount +'">\
                  <label for="farmaco">Fármaco#' + analgesicoCount +'</label> \
                  <input type="text" class="form-control" name="nomeFarmaco' + analgesicoCount +'">\
                  <label for="doseFarmaco">Dose (UI ou mg/kg) #' + analgesicoCount +'</label>\
                  <input type="text" class="form-control" name="doseFarmaco' + analgesicoCount +'">\
                  <label for="viaAdministração">Via de Administração#' + analgesicoCount +'</label>\
                  <input type="text" class="form-control" name="viaAdministração' + analgesicoCount +'">';
    $('#listaAnalgesico').append(listaAnalgesico);

    analgesicoCount+=1
    console.log(analgesicoCount);
  });
  
  $('#btnRemoveAnalgesico').on("click", function(){
    if (analgesicoCount>1) {
      analgesicoCount--;
      $('#analgesicoForm'+analgesicoCount).remove();
    }
  });


  //ANALGESIA
  let analgesiaCount = 1;
  $('#btnAddAnalgesia').on("click", function(){
    let listaFarmaco = '<div id="analgesiaForm' + analgesiaCount +'">\
                  <label for="farmaco">Fármaco#' + analgesiaCount +'</label> \
                  <input type="text" class="form-control" name="nomeAnalgesia' + analgesiaCount +'">\
                  <label for="doseFarmaco">Dose (UI ou mg/kg) #' + analgesiaCount +'</label>\
                  <input type="text" class="form-control" name="doseAnalgesia' + analgesiaCount +'">\
                  <label for="farmaco">Frequência#' + analgesiaCount +'</label> \
                  <input type="text" class="form-control" name="analgesiaFreq' + analgesiaCount +'">\
                  <label for="viaAdministração">Via de Administração#' + analgesiaCount +'</label>\
                  <input type="text" class="form-control" name="viaAdministraçãoAnalgesia' + analgesiaCount +'">';
    $('#listaAnalgesia').append(listaFarmaco);

    analgesiaCount+=1
    console.log(analgesiaCount);
  });
  
  $('#btnRemoveAnalgesia').on("click", function(){
    if (analgesiaCount>1) {
      analgesiaCount--;
      $('#analgesiaForm'+analgesiaCount).remove();
    }
  });


    //MATERIAL
  let materialCount = 1;
  $('#btnAddMaterial').on("click", function(){
    let listaFarmaco = '<div id="materialForm' + materialCount +'">\
                  <label for="farmaco">Material biológico#' + materialCount +'</label> \
                  <input type="text" class="form-control" name="nomeMaterial' + materialCount +'">\
                  <label for="doseFarmaco">Quantidade da amostra#' + materialCount +'</label>\
                  <input type="text" class="form-control" name="quantAmostraMaterial' + materialCount +'">\
                  <label for="materialFrequencia">Frequência#' + materialCount +'</label> \
                  <input type="text" class="form-control" name="materialFreq' + materialCount +'">\
                  <label for="viaAdministração">Método de coleta#' + materialCount +'</label>\
                  <input type="text" class="form-control" name="metColetaMaterial' + materialCount +'">';
    $('#listaExtMateriaisBio').append(listaFarmaco);

    materialCount+=1
    console.log(materialCount);
  });
  
  $('#btnRemoveMaterial').on("click", function(){
    if (materialCount>1) {
      materialCount--;
      $('#materialForm'+materialCount).remove();
    }
  });




</script>
@endsection

