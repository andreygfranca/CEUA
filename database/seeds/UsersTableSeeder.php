<?php
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = Role::where('name', 'aluno')->first();
        $role_manager  = Role::where('name', 'pesquisador')->first();
        $role_admin  = Role::where('name', 'admin')->first();
        $employee = new User();
        $employee->name = 'aluno';
        $employee->email = 'aluno@example.com';
        $employee->password = bcrypt('1234');
        $employee->save();
        $employee->roles()->attach($role_employee);
        $manager = new User();
        $manager->name = 'pesquisador';
        $manager->email = 'pesquisador@example.com';
        $manager->password = bcrypt('1234');
        $manager->save();
        $manager->roles()->attach($role_manager);

        $admin = new User();
        $admin->name = 'admin';
        $admin->email = 'admin@example.com';
        $admin>password = bcrypt('1234');
        $admin->save();
        $admin->roles()->attach($role_admin);
        
    }
}
