<?php
use App\User;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return view('auth.login');
});

Route::get('/aluno', 'AlunoController@index')->middleware('role:aluno');   
Route::get('/pesquisador', 'PesquisadorController@index')->middleware('role:pesquisador');
Route::get('/admin', 'AdminController@index')->middleware('role:admin');


Route::get('/pesquisador/formulario-protocolo', 'ProtocolosPesquisaController@index')->middleware('role:pesquisador');
Route::get('/pesquisador/projetos', 'ProjetoController@index')->middleware('role:pesquisador');





Auth::routes();
