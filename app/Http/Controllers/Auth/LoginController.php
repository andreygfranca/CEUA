<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    protected function authenticated()
    {
        $id=Auth::id();

        $user = User::find($id);

        if($user->authorizeRoles('aluno')) {
            return redirect('/aluno');
        }

        else if($user->authorizeRoles('pesquisador')) {
            return redirect('/pesquisador');
        }

        else if($user->authorizeRoles('administrador')) {
            return redirect('/admin');
        }

        else if($user->authorizeRoles('secretaria')) {
            return redirect('/secretaria');
        }

        return redirect('login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
