<?php

namespace App\Http\Controllers;

use App\Models\Pesquisador;
use Illuminate\Http\Request;

class PesquisadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.pesquisador.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pesquisador  $pesquisador
     * @return \Illuminate\Http\Response
     */
    public function show(Pesquisador $pesquisador)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pesquisador  $pesquisador
     * @return \Illuminate\Http\Response
     */
    public function edit(Pesquisador $pesquisador)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pesquisador  $pesquisador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pesquisador $pesquisador)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pesquisador  $pesquisador
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pesquisador $pesquisador)
    {
        //
    }
}
