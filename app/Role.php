<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    
    /* many-to-many relation with User tables*/
    public function users()
	{
	  return $this->belongsToMany(User::class);
	}
}
